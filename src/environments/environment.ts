// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const firebaseConfig = {
  apiKey: 'AIzaSyDm31r3PQUbr3pG2zBS8jeoVOwlplBvUPk',
  authDomain: 'aarai-fish.firebaseapp.com',
  databaseURL: 'https://aarai-fish.firebaseio.com',
  projectId: 'aarai-fish',
  storageBucket: 'aarai-fish.appspot.com',
  messagingSenderId: '518451836373',
  appId: '1:518451836373:web:6761f25cf8f7655a03f95c',
  measurementId: 'G-GZL4MWRS2Y'
};

export const environment = {
  production: false,
  firebase: firebaseConfig
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
