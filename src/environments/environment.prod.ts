const firebaseConfig = {
  apiKey: 'AIzaSyDm31r3PQUbr3pG2zBS8jeoVOwlplBvUPk',
  authDomain: 'aarai-fish.firebaseapp.com',
  databaseURL: 'https://aarai-fish.firebaseio.com',
  projectId: 'aarai-fish',
  storageBucket: 'aarai-fish.appspot.com',
  messagingSenderId: '518451836373',
  appId: '1:518451836373:web:6761f25cf8f7655a03f95c',
  measurementId: 'G-GZL4MWRS2Y'
};

export const environment = {
  production: true,
  firebase: firebaseConfig
};
