import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  destroy$ = new Subject();

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private firebaseAuth: AngularFireAuth) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });

    this.loginToList();
  }

  login() {
    const user = this.loginForm.value;
    this.authService.login(user.email, user.password);
  }

  loginToList() {
    this.firebaseAuth.authState.pipe(takeUntil(this.destroy$)).subscribe(user => {
      // console.log(user);
      if (user) {
        this.router.navigateByUrl('/waitlist');
      }
    });
  }

  ngDestroy() {
    this.destroy$.unsubscribe();
  }

}
