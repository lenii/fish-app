import { Component, OnInit, OnDestroy } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CustomerService } from '../core/services/customer.service';
import { getCustomerVo } from '../components/add-customer/util';
import { AddProductComponent } from '../components/add-product/add-product.component';
import { ProductsService } from '../core/services/product.service';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { UsersQueryService } from '../core/query/users/users-query.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  addVisitModalRef: BsModalRef;
  destroyed$ = new Subject();
  isLoading: boolean;
  customers: unknown[];
  products: any;
  userInfoFromLocalStorage = localStorage.getItem('userInfo');

  // TODO: Have to implement logout unsubscription
  constructor(
    private modalService: BsModalService,
    private productService: ProductsService,
    private firebaseAuth: AngularFireAuth,
    private userQueryService: UsersQueryService,
    private router: Router
  ) {
    this.firebaseAuth.authState.pipe(takeUntil(this.destroyed$)).subscribe(user => {
      if (!user) {
        this.router.navigateByUrl('/login');
      }
    });
  }

  ngOnInit() {
    this.getProducts();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.unsubscribe();
  }


  getProducts() {
    if (!this.userInfoFromLocalStorage) {
      this.firebaseAuth.authState.pipe(take(1)).subscribe(authState => {
        this.userQueryService.getUser(authState.uid).subscribe(data => {
          const user = data.payload.data();
          this.getProductsFromDB(user['accountId']);
        });
      });
    } else {
      const user = JSON.parse(this.userInfoFromLocalStorage);
      this.getProductsFromDB(user['accountId']);
    }
  }

  getProductsFromDB(accountId) {
    this.productService.getProducts(accountId)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(data => {
        this.isLoading = true;
        this.products = data;
      });
  }

  addProduct(state?) {
    let initialState = state;
    if (!state) {
      initialState = {
        products: this.products,
        title: 'Add product'
      };
    }

    this.addVisitModalRef = this.modalService.show(AddProductComponent, { initialState });
    // this.addVisitModalRef.content.closeBtnName = 'Close';
  }

  updateProduct(product) {
    const data = { active: !product.active };
    const productId = product.id;
    this.productService.updateProduct(data, productId);
  }

  editProduct(product) {
    const initialState = {
      product: product,
      title: 'Edit product'
    };
    this.addVisitModalRef = this.modalService.show(AddProductComponent, { initialState });
  }
}
