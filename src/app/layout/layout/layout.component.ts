import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import { take } from 'rxjs/operators';
import { UsersQueryService } from '../../core/query/users/users-query.service';
import { SharedService } from '../../core/services/shared-service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(
    public userQueryService: UsersQueryService,
    private firebaseAuth: AngularFireAuth,
    private sharedService: SharedService) { }

  ngOnInit() {
    this.firebaseAuth.authState.pipe(take(1)).subscribe(authState => {
      this.userQueryService.getUser(authState.uid).subscribe(data => {
        const user = data.payload.data();
        this.sharedService.userInfo = user;
        localStorage.setItem('userInfo', JSON.stringify(user));
      });
    });
  }
}
