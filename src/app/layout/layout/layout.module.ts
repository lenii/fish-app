import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';

import { AvatarModule } from 'ngx-avatar';
import { RouterModule } from '@angular/router';
import { LayoutRoutingModule } from './layout.routing';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';

import { WaitlistComponent } from '../../waitlist/waitlist.component';
import { SpinnerComponent } from '../../components/spinner/spinner.component';
import { ProductsComponent } from '../../products/products.component';
import { SettingsComponent } from '../../components/settings/settings.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [LayoutComponent, NavBarComponent, WaitlistComponent,
    SpinnerComponent, ProductsComponent, SettingsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AvatarModule,
    RouterModule,
    LayoutRoutingModule,
  ]
})
export class LayoutModule { }
