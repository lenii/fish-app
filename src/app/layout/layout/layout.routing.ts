import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { LayoutComponent } from './layout.component';
import { WaitlistComponent } from '../../waitlist/waitlist.component';
import { ProductsComponent } from '../../products/products.component';
import { SettingsComponent } from '../../components/settings/settings.component';

const routes: Routes = [
  // { path: '', redirectTo: 'waitlist', pathMatch: 'full' },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'waitlist',
        component: WaitlistComponent
      },
      {
        path: 'products',
        component: ProductsComponent
      }
      ,
      {
        path: 'settings',
        component: SettingsComponent
      }
    ]
  }
];

export const LayoutRoutingModule: ModuleWithProviders = RouterModule.forChild(
  routes
);
