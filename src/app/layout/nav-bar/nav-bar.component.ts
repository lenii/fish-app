import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  menuItem = [
    {
      name: 'products',
      url: 'products',
      class: 'sub_icon glyphicon glyphicon-th-large'
    },
    {
      name: 'waitlist',
      url: 'waitlist',
      class: 'sub_icon glyphicon glyphicon-th-list'
    },
    {
      name: 'settings',
      url: 'settings',
      class: 'sub_icon glyphicon glyphicon-wrench'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  toggleMenu(e) {
    e.preventDefault();
    const element = document.getElementById('wrapper');
    element.classList.toggle('active');
  }

}
