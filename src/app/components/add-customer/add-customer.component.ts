import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { addCustomerDBO } from './util';
import { CustomerService } from '../../core/services/customer.service';
import { SettingsService } from '../../core/services/settings.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  // modalRef: BsModalRef;
  title: string;
  list = [];
  customer: any;
  products: any;
  quantity = [{ value: 500, qty: '0.5 kg' }, { value: 1000, qty: '1kg' }, { value: 1500, qty: '1.5kg' }, { value: 2000, qty: '2kg' }];
  addCustomerForm: FormGroup;
  control1 = 'name';
  isEdit: boolean;
  destroyed$ = new Subject();
  settings: any;
  disableDays: any[];

  constructor(public formbuilder: FormBuilder,
    private customerService: CustomerService,
    public modalRef: BsModalRef,
    private settingsService: SettingsService) { }

  ngOnInit() {
    this.buildForm();
    this.getSettings();
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.unsubscribe();
  }

  getSettings() {
    const user = JSON.parse(localStorage.getItem('userInfo'));
    this.settingsService.getAccountInfo(user.accountId)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(settings => {
        this.settings = settings.payload.data();

        this.getDisabledDate();
      });
  }

  getDisabledDate() {
    const days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    const weekDays = this.settings.hours;
    this.disableDays = [];
    for (const [index, day] of days.entries()) {
      if (!weekDays[day]) {
        this.disableDays.push(index);
      }
    }
  }

  buildForm() {
    this.addCustomerForm = this.formbuilder.group({
      phone: [],
      name: [],
      deliveryDate: [],
      doorNumber: [],
      street: [],
      area: [],
      district: [],
      landMark: [],
      products: [],
      qty: this.formbuilder.array([]),
      address: []
    });

    if (this.customer && Object.keys(this.customer).length > 0) {
      this.isEdit = true;
      this.addCustomerForm.patchValue(this.customer);
      this.selectProduct(this.customer.products);
    } else {
      this.isEdit = false;
    }
  }

  deselectProduct(product) {
    const productQtyGroup = this.addCustomerForm.get('qty') as FormArray;
    const prodIndex = product.index;
    productQtyGroup.removeAt(prodIndex);
  }

  selectProduct(products) {
    const productQtyGroup = this.addCustomerForm.get('qty') as FormArray;
    const prods = products || [];
    for (const prod of prods) {
      productQtyGroup.push(this.createArray(prod));
    }

    console.log(productQtyGroup);
  }

  createArray(product) {
    return this.formbuilder.group({
      productQty: [product ? product.qty ? product.qty : 500 : 500],
      productName: product
    });
  }

  dynamicFormArray(productArray) {
    const temp = [];
    const products = productArray || [];
    for (const product of products) {
      temp.push(this.formbuilder.group({ productQty: [product ? product.qty : 500] }));
    }

    return temp;
  }

  save() {
    const dbo = addCustomerDBO(this.addCustomerForm.value);
    if (!this.isEdit) {
      this.customerService.addCustomer(dbo);
    } else {
      const customerId = this.customer.id;
      this.customerService.updateCustomer(dbo, customerId);
    }

    this.modalRef.hide();
  }

}
