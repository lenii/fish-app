
export function addCustomerDBO(data) {
  const products = [];
  for (const product of data.qty) {
    const prodObj = {
      productId: product.productName.id,
      productName: product.productName.itemName,
      // tslint:disable-next-line: radix
      qty: parseInt(product.productQty)
    };
    products.push(prodObj);
  }
  return {
    name: data.name,
    date: new Date(),
    deliveryDate: getFormattedDate(data.deliveryDate),
    phone: data.phone,
    doorNumber: data.doorNumber,
    street: data.street,
    area: data.area,
    district: data.district,
    landMark: data.landMark,
    products: products
  };
}


function getFormattedDate(date) {
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const current_datetime = new Date(date);
  const formatted_date = current_datetime.getDate() + '-' + months[current_datetime.getMonth()] + '-' + current_datetime.getFullYear();
  return formatted_date;
}

export function getCustomerVo(customers, products?) {
  // console.log(products);
  for (const customer of customers) {
    customer.productRate = [];
    let rate = 0;
    for (const product of customer.products) {
      product.id = product.productId;
      product.itemName = product.productName;
      product.qtyInKg = product.qty / 1000 + ' Kg';

      // tslint:disable-next-line: radix
      rate = rate + (getProductRate(products, product.id, product.qty));
      customer.productRate.push(getProductRate(products, product.id, product.qty));
    }
    customer['total'] = rate;
  }
  // console.log(customers);
  return customers;
}

export function getProductRate(products, prodId, qty) {
  for (const prod of products) {
    if (prod.id === prodId) {
      // tslint:disable-next-line: radix
      const ratePerGram = parseInt(prod.rate) / 1000;
      return qty * ratePerGram;
    }
  }
}
