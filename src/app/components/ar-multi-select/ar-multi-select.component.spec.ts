import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArMultiSelectComponent } from './ar-multi-select.component';

describe('ArMultiSelectComponent', () => {
  let component: ArMultiSelectComponent;
  let fixture: ComponentFixture<ArMultiSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArMultiSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
