import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ar-multi-select',
  templateUrl: './ar-multi-select.component.html',
  styleUrls: ['./ar-multi-select.component.scss']
})
export class ArMultiSelectComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input() controlName: string;
  @Output() selectProduct = new EventEmitter<any[]>();
  @Output() deselectProduct = new EventEmitter<any[]>();
  @Input() title: string;
  customerInfo: any;
  prodItems: any;
  @Input('customer')
  set customer(value) {
    this.customerInfo = value;
    this.selectedItems = value.products || [];
  }

  @Input('products')
  set products(values) {
    if (values) {
      const data = values || [];
      this.getProductVo(data);
    }
  }

  selectedItems = [];

  dropdownSettings = {
    singleSelection: false,
    text: 'Select product',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
  };

  constructor() { }

  ngOnInit() {
  }

  getProductVo(products) {
    console.log(products);
    this.prodItems = [];
    for (const [index, product] of products.entries()) {
      this.prodItems.push(
        { index: index, id: product.id, itemName: product.productName, rate: product.rate }
      );
    }
  }

  onItemSelect(item: any) {
    // console.log(item);
    // console.log(this.selectedItems);
    this.selectProduct.emit([item]);
  }
  OnItemDeSelect(item: any) {
    // console.log(item);
    // console.log(this.selectedItems);
    this.deselectProduct.emit(item);
  }
  onSelectAll(items: any) {
    // console.log(items);
  }
  onDeSelectAll(items: any) {
    // console.log(items);
  }

}
