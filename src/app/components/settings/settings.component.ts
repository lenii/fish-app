import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService } from '../../core/services/settings.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { UsersQueryService } from '../../core/query/users/users-query.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject();
  daysForm: FormGroup;
  minNoticeForm: FormGroup;
  userInfoFromLocalStorage = localStorage.getItem('userInfo');

  minNotice = ['1 day', '2 days', '3 days', '4 days', '5 days', '6 days', '7 days', '8 days', '9 days', '10 days'];

  minQty = [{ name: '0.5 kg', value: 500 }, { name: '1 kg', value: 1000 }, { name: '1.5 kg', value: 1500 }, { name: '2 kg', value: 2000 }];

  days = [
    {
      day: 'Sunday', constant: 'SUN'
    },
    {
      day: 'Monday', constant: 'MON'
    },
    {
      day: 'Tuesday', constant: 'TUE'
    },
    {
      day: 'Wednesday', constant: 'WED'
    },
    {
      day: 'Thursday', constant: 'THU'
    },
    {
      day: 'Friday', constant: 'FRI'
    },
    {
      day: 'Saturday', constant: 'SAT'
    }
  ];

  hours = {};
  settings: any;
  isLoading: boolean;
  accountId: any;
  updateSettings: boolean;
  constructor(
    private settingsService: SettingsService,
    public formbuilder: FormBuilder,
    private userQueryService: UsersQueryService,
    private firebaseAuth: AngularFireAuth) { }

  ngOnInit() {
    this.buildForm();
    this.getAccountInfo();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.unsubscribe();
  }

  buildForm() {
    this.daysForm = this.formbuilder.group({
      sun: [],
      mon: [],
      tue: [],
      wed: [],
      thu: [],
      fri: [],
      sat: []
    });

    this.minNoticeForm = this.formbuilder.group({
      minNotice: [],
      minQty: []
    });
  }

  getSettings(accountId) {
    this.settingsService.getAccountInfo(accountId).pipe(takeUntil(this.destroyed$)).subscribe((data: any) => {
      if (data) {
        this.isLoading = true;
        this.settings = data.payload.data();
        if (this.settings) {
          this.updateSettings = true;
          this.hours = this.settings.hours || {};
          this.daysForm.patchValue(this.hours);
          this.minNoticeForm.patchValue(this.settings);
          this.initAvailableDays();
        }
      }
    });
  }

  getAccountInfo() {
    if (!this.userInfoFromLocalStorage) {
      this.firebaseAuth.authState.pipe(take(1)).subscribe(authState => {
        this.userQueryService.getUser(authState.uid).subscribe(data => {
          const user = data.payload.data();
          this.getSettings(user['accountId']);
          this.accountId = user['accountId'];
        });
      });
    } else {
      const user = JSON.parse(this.userInfoFromLocalStorage);
      this.accountId = user['accountId'];
      this.getSettings(user['accountId']);
    }
  }

  initAvailableDays() {
    for (const day of this.days) {
      const dayConst = day.constant.toLowerCase();
      if (this.hours && this.hours[dayConst]) {
        day['active'] = true;
      }
    }
  }

  changeAvailability(event) {
    const open = event.target.checked;
    this.settingsService.updateAccountInfo(this.accountId, { allowPublicBooking: open }, this.updateSettings);
  }

  changeToggle(day, event) {
    day.active = day ? !day.active : false;
    this.settingsService.updateAccountInfo(this.accountId, { hours: this.daysForm.value }, this.updateSettings);
  }

  selectMin(event, attribute) {
    const value = event.target.value;
    this.settingsService.updateAccountInfo(this.accountId, { [attribute]: value }, this.updateSettings);
  }

}
