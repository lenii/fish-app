import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ar-fish-text-area',
  templateUrl: './ar-fish-text-area.component.html',
  styleUrls: ['./ar-fish-text-area.component.scss']
})
export class ArFishTextAreaComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input() controlName: string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
