import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArFishTextAreaComponent } from './ar-fish-text-area.component';

describe('ArFishTextAreaComponent', () => {
  let component: ArFishTextAreaComponent;
  let fixture: ComponentFixture<ArFishTextAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArFishTextAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArFishTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
