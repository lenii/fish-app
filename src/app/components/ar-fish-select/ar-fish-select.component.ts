import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ar-fish-select',
  templateUrl: './ar-fish-select.component.html',
  styleUrls: ['./ar-fish-select.component.scss']
})
export class ArFishSelectComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() placeholder: string;
  @Input() controlName: string;
  @Input() title: string;
  prods: any;

  @Output() selectedProduct = new EventEmitter<string>();

  @Input('products')
  set products(value) {
    this.prods = value;
    console.log('==== : ', value);
    if (this.form) {
      this.form.patchValue({ product: value[0]['id'] });
    }
  }

  constructor() { }

  ngOnInit() {
  }

  selectProduct(value) {
    this.selectedProduct.emit(value.target.value);
  }

}
