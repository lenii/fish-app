import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArFishSelectComponent } from './ar-fish-select.component';

describe('ArFishSelectComponent', () => {
  let component: ArFishSelectComponent;
  let fixture: ComponentFixture<ArFishSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArFishSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArFishSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
