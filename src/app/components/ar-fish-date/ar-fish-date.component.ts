import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SettingsService } from '../../core/services/settings.service';
import { takeUntil, take } from 'rxjs/operators';

@Component({
  selector: 'app-ar-fish-date',
  templateUrl: './ar-fish-date.component.html',
  styleUrls: ['./ar-fish-date.component.scss']
})
export class ArFishDateComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input() controlName: string;
  @Input() title: string;
  @Input() disableDays: any;

  date = new Date();
  minDate: any;
  settings: any;

  constructor(private settingsService: SettingsService) { }

  ngOnInit() {
    // tslint:disable-next-line: radix
    const user = JSON.parse(localStorage.getItem('userInfo'));
    this.settingsService.getAccountInfo(user.accountId)
      .pipe(take(1))
      .subscribe(settings => {
        this.settings = settings.payload.data();
        const addDate = this.settings && this.settings['minNotice'] ? parseInt(this.settings['minNotice'], 10) : 1;
        this.minDate = new Date(this.date.setDate(this.date.getDate() + addDate));
        console.log(this.minDate);
      });
  }

}
