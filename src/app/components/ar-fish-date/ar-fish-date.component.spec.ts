import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArFishDateComponent } from './ar-fish-date.component';

describe('ArFishDateComponent', () => {
  let component: ArFishDateComponent;
  let fixture: ComponentFixture<ArFishDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArFishDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArFishDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
