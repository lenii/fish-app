import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArFishNameComponent } from './ar-fish-name.component';

describe('ArFishNameComponent', () => {
  let component: ArFishNameComponent;
  let fixture: ComponentFixture<ArFishNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArFishNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArFishNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
