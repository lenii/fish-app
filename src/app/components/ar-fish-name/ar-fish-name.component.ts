import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ar-fish-name',
  templateUrl: './ar-fish-name.component.html',
  styleUrls: ['./ar-fish-name.component.scss']
})
export class ArFishNameComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input() controlName: string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }



}
