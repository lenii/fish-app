import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductsService } from '../../core/services/product.service';
import { addProductDBO } from './util';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;
  product: any;
  isEdit: boolean;
  title = 'Add product';

  constructor(public formbuilder: FormBuilder, private productService: ProductsService, public modalRef: BsModalRef) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.addProductForm = this.formbuilder.group({
      product: [],
      rate: [],
    });

    if (this.product && Object.keys(this.product).length > 0) {
      this.title = 'Update product';
      this.isEdit = true;
      this.addProductForm.patchValue({ product: this.product.productName, rate: this.product.rate });
    } else {
      this.isEdit = false;
    }
  }

  validateNumeric(event) {
    if (event.shiftKey) {
      return false;
    } // Denying the sift key
    // tslint:disable-next-line: deprecation
    const key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 37) {
      // 46 - Dot character (.)
      return true;
    } else if (key < 48 || key > 57) {
      return false;
    } else {
      return true;
    }
  }

  save() {
    if (!this.isEdit) {
      this.productService.addProduct(addProductDBO(this.addProductForm.value));
    } else {
      const data = this.addProductForm.value;
      const productId = this.product.id;
      this.productService.updateProduct(data, productId);
    }

    this.modalRef.hide();
  }

}
