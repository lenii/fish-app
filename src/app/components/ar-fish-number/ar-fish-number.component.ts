import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ar-fish-number',
  templateUrl: './ar-fish-number.component.html',
  styleUrls: ['./ar-fish-number.component.scss']
})
export class ArFishNumberComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input() controlName: string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

  validateNumeric(event) {
    if (event.shiftKey) {
      return false;
    } // Denying the sift key
    const key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 37) {
      // 46 - Dot character (.)
      return true;
    } else if (key < 48 || key > 57) {
      return false;
    } else {
      return true;
    }
  }

}
