import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArFishNumberComponent } from './ar-fish-number.component';

describe('ArFishNumberComponent', () => {
  let component: ArFishNumberComponent;
  let fixture: ComponentFixture<ArFishNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArFishNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArFishNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
