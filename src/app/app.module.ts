import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { AuthService } from './core/services/auth.service';
import { SignupComponent } from './signup/signup.component';
import { AddCustomerComponent } from './components/add-customer/add-customer.component';
import { ArFishNameComponent } from './components/ar-fish-name/ar-fish-name.component';
import { ArFishTextAreaComponent } from './components/ar-fish-text-area/ar-fish-text-area.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ArFishDateComponent } from './components/ar-fish-date/ar-fish-date.component';
import { ArMultiSelectComponent } from './components/ar-multi-select/ar-multi-select.component';
import { CustomerService } from './core/services/customer.service';
import { AddProductComponent } from './components/add-product/add-product.component';
import { ArFishSelectComponent } from './components/ar-fish-select/ar-fish-select.component';
import { ProductsService } from './core/services/product.service';
import { ArFishNumberComponent } from './components/ar-fish-number/ar-fish-number.component';
import { SettingsService } from './core/services/settings.service';
import { PublicPageComponent } from './public-page/public-page.component';
import { SharedService } from './core/services/shared-service';
// import { SettingsComponent } from './components/settings/settings.component';
// import { ProductsComponent } from './products/products.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    AddCustomerComponent,
    ArFishNameComponent,
    ArFishTextAreaComponent,
    ArFishDateComponent,
    ArMultiSelectComponent,
    AddProductComponent,
    ArFishSelectComponent,
    ArFishNumberComponent,
    PublicPageComponent,
    // SettingsComponent,
    // ProductsComponent
  ],
  imports: [
    BrowserModule, BrowserModule, FormsModule,
    ButtonsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularMultiSelectModule
  ],
  entryComponents: [AddCustomerComponent, AddProductComponent],
  providers: [AngularFireAuth, AuthService, CustomerService, ProductsService, SettingsService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
