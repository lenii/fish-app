

export function getUserDbo(user, name, phone) {
  return {
    created: new Date(),
    name: name,
    email: user.email,
    phone: phone,
    uid: user.uid
  };
}
