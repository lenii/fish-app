import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { SharedService } from '../../services/shared-service';

@Injectable({
  providedIn: 'root'
})
export class CustomerQueryService {

  constructor(public afs: AngularFirestore, public router: Router, private sharedService: SharedService) { }

  addCustomer(customer) {
    const accountId = this.sharedService.userInfo.accountId;
    this.afs.collection('account').doc(accountId).collection('customers').add(customer)
      .then((docRef) => {
        console.log('success');
      })
      .catch((error) => {
        console.error('Error creating user', error);
      });
  }

  updateCustomer(data, customerId) {
    const accountId = this.sharedService.userInfo.accountId;
    this.afs.collection('account').doc(accountId).collection('customers').doc(customerId).update(data).then(() => console.log('SUCCESS!'))
      .catch((err) => console.log(err));
  }

  getCustomers(accountId) {
    return this.afs.collection('account').doc(accountId).collection('customers', ref => {
      return ref.orderBy('date');
    }).snapshotChanges()
      .pipe(
        map(customers => {
          const result = customers.map(data => {
            return {
              ...data.payload.doc.data(),
              id: data.payload.doc.id,
            };
          });

          return result;
        })
      );
    // }
  }
}
