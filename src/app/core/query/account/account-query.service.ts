import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountQueryService {

  constructor(public afs: AngularFirestore, public router: Router) { }

  getAccountInfo(accountId) {
    return this.afs.collection('account').doc(accountId).snapshotChanges();
  }

  updateAccount(accountId, data, updateSttings) {
    if (updateSttings) {
      return this.afs.collection('account').doc(accountId).update(data);
    } else {
      return this.afs.collection('account').doc(accountId).set(data);
    }
  }
}
