import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { SharedService } from '../../services/shared-service';

@Injectable({
  providedIn: 'root'
})
export class ProductsQueryService {

  constructor(public afs: AngularFirestore, public router: Router, private sharedService: SharedService) { }

  addProduct(product) {
    const accountId = this.sharedService.userInfo.accountId;
    this.afs.collection('account').doc(accountId).collection('products').add(product)
      .then((docRef) => {
        console.log('success');
      })
      .catch((error) => {
        console.error('Error creating product : ', error);
      });
  }

  updateProduct(data, productId) {
    const accountId = this.sharedService.userInfo.accountId;
    this.afs.collection('account').doc(accountId).collection('products').doc(productId).update(data)
      .then((docRef) => {
        console.log('success');
      })
      .catch((error) => {
        console.error('Error updating product : ', error);
      });
  }

  getProducts(accountId, activeProduct?) {
    return this.afs.collection('account').doc(accountId).collection('products', ref => {
      if (!activeProduct) {
        return ref.orderBy('date');
      } else {
        return ref.where('active', '==', true);
      }
    }).snapshotChanges()
      .pipe(
        map(customers => {
          const result = customers.map(data => {
            return {
              ...data.payload.doc.data(),
              id: data.payload.doc.id,
            };
          });

          return result;
        })
      );
  }
}
