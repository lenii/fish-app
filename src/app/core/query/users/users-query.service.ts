import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class UsersQueryService {

  constructor(public afs: AngularFirestore, public router: Router, private firebaseAuth: AngularFireAuth) { }

  addUser(user) {
    try {
      return this.afs.collection('users').doc(user.uid).set(user)
        .then((docRef) => {
          const accountId = this.afs.createId();
          this.afs.collection('users').doc(user.uid).update({ accountId: accountId })
            .then(() => {
              console.log('SUCCESS!');
              this.router.navigateByUrl('/login');
            })
            .catch(err => {
              console.log('update account id to user failed!');
            });
        })
        .catch((error) => {
          console.error('Error creating user', error);
        })
        .then(ref => {
          console.log('==============================');
        });
    } catch (err) {
      console.log(err);
    }

  }

  getUser(userId) {
    return this.afs.collection('users').doc(userId).snapshotChanges();
  }
}
