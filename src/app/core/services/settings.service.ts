import { Injectable } from '@angular/core';
import { AccountQueryService } from '../query/account/account-query.service';
import { ToastService } from '../../toast.service';

@Injectable()
export class SettingsService {

  constructor(private accountQueryService: AccountQueryService, private toastr: ToastService) { }

  getAccountInfo(accountId) {
    return this.accountQueryService.getAccountInfo(accountId);
  }

  updateAccountInfo(accountId, data, updateSettings) {
    this.accountQueryService.updateAccount(accountId, data, updateSettings).catch(e => {
      this.toastr.showError(e);
    });
  }
}
