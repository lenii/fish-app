import { Injectable } from '@angular/core';
import { CustomerQueryService } from '../query/customers/customer-query.service';

@Injectable()
export class CustomerService {

  constructor(private customerQueryService: CustomerQueryService) {
  }

  addCustomer(customer) {
    this.customerQueryService.addCustomer(customer);
  }

  updateCustomer(data, customerId) {
    this.customerQueryService.updateCustomer(data, customerId);
  }

  getCustomers(accountId) {
    return this.customerQueryService.getCustomers(accountId);
  }
}
