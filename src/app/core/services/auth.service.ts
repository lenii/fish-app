import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';
import { ToastService } from '../../toast.service';
import { UsersQueryService } from '../query/users/users-query.service';
import { getUserDbo } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;

  constructor(
    private firebaseAuth: AngularFireAuth,
    private toastr: ToastService,
    private usersQueryService: UsersQueryService,
    private router: Router) {
    this.user = firebaseAuth.authState;
  }

  signup(user) {
    const email = user.email;
    const password = user.password;
    this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        const val = { name: user.name, email: value.user.email, uid: value.user.uid, phone: user.phone };
        // const authUser = this.firebaseAuth.auth.currentUser;
        this.usersQueryService.addUser(val);
      })
      .catch(err => {
        this.toastr.showError(err.message);
      });
  }

  login(email: string, password: string) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        this.toastr.showSuccess('Success!');
      })
      .catch(err => {
        this.toastr.showError(err.message);
      });
  }

  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

}
