import { Injectable } from '@angular/core';
import { ProductsQueryService } from '../query/products/product-query.service';
// import { CustomerQueryService } from '../query/customers/customer-query.service';

@Injectable()
export class ProductsService {

  constructor(private productQueryService: ProductsQueryService) {
  }

  addProduct(product) {
    this.productQueryService.addProduct(product);
  }

  updateProduct(data, customerId) {
    this.productQueryService.updateProduct(data, customerId);
  }

  getProducts(accountId, activeProduct?) {
    return this.productQueryService.getProducts(accountId, activeProduct);
  }
}
