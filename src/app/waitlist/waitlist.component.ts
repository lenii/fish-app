import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddCustomerComponent } from '../components/add-customer/add-customer.component';
import { CustomerService } from '../core/services/customer.service';
import { getCustomerVo } from '../components/add-customer/util';
import { Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { ProductsService } from '../core/services/product.service';
import { UsersQueryService } from '../core/query/users/users-query.service';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-waitlist',
  templateUrl: './waitlist.component.html',
  styleUrls: ['./waitlist.component.scss']
})
export class WaitlistComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject();
  addVisitModalRef: BsModalRef;
  isLoading: boolean;
  customers: unknown[];
  products: any[];
  userInfoFromLocalStorage = localStorage.getItem('userInfo');

  // TODO: Have to implement logout unsubscription
  constructor(
    private modalService: BsModalService,
    private customerService: CustomerService,
    private productService: ProductsService,
    private userQueryService: UsersQueryService,
    private firebaseAuth: AngularFireAuth
  ) {
  }

  ngOnInit() {
    this.getActiveProducts();
    this.getCustomers();
  }

  getCustomers() {
    if (!this.userInfoFromLocalStorage) {
      this.firebaseAuth.authState.pipe(take(1)).subscribe(authState => {
        this.userQueryService.getUser(authState.uid).subscribe(data => {
          const user = data.payload.data();
          this.getCustomersFromDB(user['accountId']);
        });
      });
    } else {
      const user = JSON.parse(this.userInfoFromLocalStorage);
      this.getCustomersFromDB(user['accountId']);
    }
  }

  getCustomersFromDB(accountId) {
    this.customerService.getCustomers(accountId).pipe(takeUntil(this.destroyed$)).subscribe(customers => {
      this.isLoading = true;
      this.customers = getCustomerVo(customers, this.products);
      console.log(customers);
    });
  }

  getActiveProducts() {
    if (!this.userInfoFromLocalStorage) {
      this.firebaseAuth.authState.pipe(take(1)).subscribe(authState => {
        this.userQueryService.getUser(authState.uid).subscribe(data => {
          const user = data.payload.data();
          this.getProductsFromDB(user['accountId']);
        });
      });
    } else {
      const user = JSON.parse(this.userInfoFromLocalStorage);
      this.getProductsFromDB(user['accountId']);
    }
  }

  getProductsFromDB(accountId) {
    this.productService.getProducts(accountId, true)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(data => {
        this.isLoading = true;
        this.products = data;
      });
  }

  ngOnDestroy() {
    this.destroyed$.unsubscribe();
  }

  addCustomer(state?) {
    let initialState = state;
    if (!state) {
      initialState = {
        list: [
          'Open a modal with component',
          'Pass your data',
          'Do something else',
          '...'
        ],
        products: this.products,
        customer: {},
        title: 'Add customer'
      };
    }

    this.addVisitModalRef = this.modalService.show(AddCustomerComponent, { initialState });
    // this.addVisitModalRef.content.closeBtnName = 'Close';
  }

  updateCustomer(customer) {
    const data = { state: 'SERVED' };
    const customerId = customer.id;
    this.customerService.updateCustomer(data, customerId);
  }

  undo(customer) {
    const data = { state: null };
    const customerId = customer.id;
    this.customerService.updateCustomer(data, customerId);
  }

  editCustomer(customer) {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      products: this.products,
      customer: customer,
      title: 'Edit customer'
    };
    // this.addVisitModalRef = this.modalService.show(AddCustomerComponent, { initialState });
    this.addCustomer(initialState);
  }
}
